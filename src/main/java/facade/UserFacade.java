package facade;

import javax.transaction.Transactional;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import dto.UpdateUserDto;
import exception.InvalidUserDtoException;
import hello.User;

public class UserFacade {
	
	@Transactional
	public void updateUser(User user, UpdateUserDto dto) throws InvalidUserDtoException {
		System.out.println("Test");
		System.out.println("Test");
		System.out.println("Test");
		System.out.println("Test");
		System.out.println("Test");
		System.out.println("Test");
		
		user.setEmail(dto.getEmail());
		user.setName(dto.getName());
 	};
	
	boolean isValid(User user) {
		if(user.getEmail() == null || user.getName() == null) {
			return false;
		}
		return true;
	}
}
